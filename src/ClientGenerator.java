import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ClientGenerator implements Runnable{
	private List<Queue> queueList;
	private List<Thread> threadList;
	private int minArrive;
	private int maxArrive; 
	private int minServiceTime;
	private int maxServiceTime;
	private Date startDate; 
	private Date endDate;
	private Date currentDate;
	private boolean running;
	private PrintWriter writer;
	private int maxBusyTimeSum=0;
	private Date peakHour = new Date();
	
	public ClientGenerator(int minArrive, int maxArrive, int minServiceTime, int maxServiceTime,
			int numberOfQueues, Date startDate, Date endDate) {
		try{
		     writer = new PrintWriter("logfile.txt", "UTF-8");
		} catch (IOException e) {
		   	System.out.println("Error opening file");
		}
		
		queueList= new ArrayList<Queue>();
		for (int i=0; i<numberOfQueues; i++) {
			queueList.add(new Queue(startDate, endDate, writer));
		}
		this.minArrive=minArrive;
		this.maxArrive=maxArrive;
		this.minServiceTime=minServiceTime;
		this.maxServiceTime=maxServiceTime;
		this.startDate=startDate;
		this.endDate=endDate;
		
		currentDate=startDate;	
		running=true;
	}
	
	private synchronized Queue lessBusyQueue() {
		return Collections.min(queueList);
	}
	
	public void generate() {
		System.out.printf("Start date: %tT\n", startDate);
		System.out.printf("End date: %tT\n", endDate);
		
		threadList = new ArrayList<Thread>();
		for (Queue queue : queueList) {
			threadList.add(new Thread(queue));
			threadList.get(threadList.size()-1).start();
		}
		
		threadList.add(new Thread(this));
		threadList.get(threadList.size()-1).start();
	}
	
	public void run() {
		Random random = new Random();
		while (currentDate.before(endDate)) {
			int busyTimeSum=0;
			for (Queue queue : queueList) {
				busyTimeSum += queue.getBusyTime();
			}
			if (busyTimeSum > maxBusyTimeSum) {
				maxBusyTimeSum = busyTimeSum;
				peakHour.setTime(currentDate.getTime()); 
			}
			try {
				Queue currentQueue = lessBusyQueue();
				currentQueue.addClient(new Client(minServiceTime + random.nextInt(maxServiceTime - minServiceTime), currentDate));
				int randTime = (minArrive + random.nextInt(maxArrive - minArrive)) * Queue.TIME_MULTIPLIER;
				Thread.sleep(Math.min(randTime, endDate.getTime() - currentDate.getTime()));
				currentDate.setTime(currentDate.getTime() + randTime);
			} catch (InterruptedException e) {
				System.out.println("Simulation ends");
			}
		}
		for (Thread thread : threadList) {
			running = false;
			thread.interrupt();
		}
		writer.close();
	}
	
	public int getNumberOfQueues() {
		return queueList.size();
	}
	
	public String getQueueName(int index) {
		return queueList.get(index).toString();
	}
	
	public String getQueueClients(int index) {
		return queueList.get(index).toStringListClients();
	}
	
	public List<Queue> getQueueList() {
		return queueList;
	}
	
	public boolean getRunningState() {
		return running;
	}
	
	public Date getBusyUntil(int index) {
		return queueList.get(index).busyUntil();
	}
	
	public int getServiceTime(int index) {
		return queueList.get(index).getServiceTime();
	}
	
	public int getAvgWaitingTime(int index) {
		return queueList.get(index).getAvgWaitingTime();
	}
	
	public int getFreeTime(int index) {
		return queueList.get(index).getFreeTime();
	}
	
	public int getMinServiceTime() {
		return minServiceTime;
	}
	
	public int getMaxServiceTime() {
		return maxServiceTime;
	}

	public Date getPeakHour() {
		return peakHour;
	}
}
