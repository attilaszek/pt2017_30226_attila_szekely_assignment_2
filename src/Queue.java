import java.io.PrintWriter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Queue implements Runnable, Comparable<Queue> {
	private static int QUEUE_ID=0;
	public static final int TIME_MULTIPLIER=1000;
	
	private int queueID;
	private List<Client> clientList;
	private Date startDate;
	private Date endDate;
	private long currentTime=0;
	private boolean sleep = true;
	
	//Statistics
	private int serviceTime=0;
	private int waitingTime=0;
	private int freeTime=0;
	private int nrServedClients=0;
	private int busyTime=0;
	
	PrintWriter writer;

	public Queue(Date startDate, Date endDate, PrintWriter writer) {
		queueID = QUEUE_ID++;
		this.startDate = new Date(startDate.getTime());
		this.endDate = new Date(endDate.getTime());
		clientList = new LinkedList<Client>();
		this.writer = writer;
		currentTime=startDate.getTime();
	}
	
	@Override
	public void run() {
		System.out.printf("Queue[%d] opens at: %tT\n", queueID, startDate);
		writer.printf("Queue[%d] opens at: %tT\n", queueID, startDate);
		try {
			while (true) {
				removeClient();
			}
		} catch( InterruptedException e ) { 
			System.out.printf("Queue %d serves last Client at: %tT\n",queueID, currentTime);
			writer.printf("Queue %d serves last Client at: %tT\n",queueID, currentTime);
		}
	}
	
	public void closeWriter() {
		writer.close();
	}
	
	public List<Client> getClientList() {
		return clientList;
	}
	
	public void addClient(Client client) throws InterruptedException {
		if (busyUntil().getTime() + client.getTService() * TIME_MULTIPLIER < endDate.getTime()) {
			clientList.add(client);
			System.out.printf("Queue %d: Client%d arrives on: %tT (%d)\n",queueID,client.getClientID(),client.getTArrive(),client.getTService());
			writer.printf("Queue %d: Client%d arrives on: %tT (%d)\n",queueID,client.getClientID(),client.getTArrive(),client.getTService());
		}
		else {
			System.out.printf("         Client%d arrives on: %tT (%d), but cannot be served\n",client.getClientID(),client.getTArrive(),client.getTService());
			writer.printf("         Client%d arrives on: %tT (%d), but cannot be served\n",client.getClientID(),client.getTArrive(),client.getTService());
		}
		synchronized(this) {
			this.notify();
		}
	}
	
	public void removeClient() throws InterruptedException {
		synchronized(this) {
		while (sizeOfClientList() == 0) {
			sleep = true;
			wait();
		}
		}
		
		Client client = clientList.get(0);
		if (sleep && sizeOfClientList() == 1) {
			sleep = false;
			currentTime = client.getTArrive().getTime();
		}
		
		long time=client.getTService() * TIME_MULTIPLIER;
		Thread.sleep(time);
		
		waitingTime += currentTime - client.getTArrive().getTime();
		
		currentTime = currentTime + time;
		
		serviceTime += time;
		freeTime = (int) (endDate.getTime() - startDate.getTime() - serviceTime);
		nrServedClients++;
		
		clientList.remove(0);
		System.out.printf("Queue %d: Client%d served on: %tT\n",queueID,client.getClientID(),currentTime);
		writer.printf("Queue %d: Client%d served on: %tT\n",queueID,client.getClientID(),currentTime);
	}
	
	public int sizeOfClientList() {
		return clientList.size();
	}
	
	public Date busyUntil() {
		busyTime=0;
		for (Client client : clientList) {
			busyTime += client.getTService() * TIME_MULTIPLIER;
		}
		return new Date(currentTime + busyTime);
	}

	@Override
	public int compareTo(Queue queue) {
		return this.busyUntil().compareTo(queue.busyUntil());
	}
	
	public String toString() {
		return "Queue["+queueID+"]";
	}
	
	public String toStringListClients() {
		String returnValue=toString();
		for (Client client : clientList) {
			returnValue += "\n     "+client.toString();
		}
		return returnValue;     
	}
	
	public int getQueueID() {
		return queueID;
	}
	
	public int getServiceTime() {
		return serviceTime / TIME_MULTIPLIER;
	}
	
	public int getAvgWaitingTime() {
		return waitingTime / nrServedClients / TIME_MULTIPLIER;
	}
	
	public int getFreeTime() {
		return freeTime / TIME_MULTIPLIER;
	}
	
	public int getBusyTime() {
		return busyTime;
	}
	
	public long getCurrentTime() {
		return currentTime;
	}
}
