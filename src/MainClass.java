public class MainClass {
	
	public static void main(String[] args) {
		InputForm input = new InputForm();
		ClientGenerator      model      = new ClientGenerator(input.getMinArriveData(),
															input.getMaxArriveData(),
															input.getMinServiceTimeData(),
															input.getMaxServiceTimeData(),
															input.getNumberOfQueuesData(),
															input.getStartDate(),
															input.getEndDate());

		model.generate();
		
		Display view = new Display(model);
		view.run();

	}
}
