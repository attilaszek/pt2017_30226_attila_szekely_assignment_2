import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Client {
	private static int CLIENT_ID=0;
	private int clientID;
	private Date tArrive;
	private Date tFinish = null;
	private int tService;
		
	public Client(int tService, Date tArrive) {
		clientID = CLIENT_ID++;
		//this.tArrive = tArrive;
		this.tArrive = new Date(tArrive.getTime());
		this.tService = tService;
	}
	
	public Date getTArrive() {
		return tArrive;
	}
	
	public int getTService() {
		return tService;
	}
	
	public int getClientID() {
		return clientID;
	}
	
	public String toString() {
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		
		if (tFinish != null) {
			return ("Client["+clientID+"] "+tArrive.toString()+" "+tFinish.toString()+" "+tService);
		}
		return ("Client["+clientID+"] "+df.format(tArrive)+" ["+tService+"]");
	}
}
