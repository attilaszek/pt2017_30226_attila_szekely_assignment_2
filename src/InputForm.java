import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

public class InputForm extends JDialog {
	private int minArriveData;
	private int maxArriveData;
	private int minServiceTimeData;
	private int maxServiceTimeData;
	private int numberOfQueuesData;
	private Date startDate;
	private Date endDate;
	
	private JLabel minArriveLabel = new JLabel("Minimum interval of arriving time:");
	private JLabel maxArriveLabel = new JLabel("Maximum interval of arriving time:");
	private JLabel minServiceTimeLabel = new JLabel("Minimum service time:");
	private JLabel maxServiceTimeLabel = new JLabel("Maximum service time:");
	private JLabel numberOfQueuesLabel = new JLabel("Number of queues:");
	private JLabel startTimeLabel = new JLabel("Simulation start:");
	private JLabel endTimeLabel = new JLabel("Simulation end:");
	
	private JTextField minArrive = new JTextField(5);
	private JTextField maxArrive = new JTextField(5);
	private JTextField minServiceTime = new JTextField(5);
	private JTextField maxServiceTime = new JTextField(5);
	private JTextField numberOfQueues = new JTextField(5);
	private JFormattedTextField startTime;
	private JFormattedTextField endTime;
	
	private JButton submit = new JButton("Submit");
	
	public InputForm() {
		this.setModal(true);
		this.setSize(1000, 500);
		this.setLocation(500, 200);
		
		JPanel content = new JPanel();
		
		
		GridLayout layout = new GridLayout(8,2);
        content.setLayout(layout);
        
		content.add(minArriveLabel);
		content.add(minArrive);
		
		content.add(maxArriveLabel);
		content.add(maxArrive);
		
		content.add(minServiceTimeLabel);
		content.add(minServiceTime);
		
		content.add(maxServiceTimeLabel);
		content.add(maxServiceTime);
		
		content.add(numberOfQueuesLabel);
		content.add(numberOfQueues);
		
		MaskFormatter mask = null;
        try {
            mask = new MaskFormatter("##:##:##");
            mask.setPlaceholderCharacter('#');
        } catch (java.text.ParseException e) {
			e.printStackTrace();
		}

        startTime = new JFormattedTextField(mask);
        content.add(startTimeLabel);
        content.add(startTime);
        
        endTime = new JFormattedTextField(mask);
        content.add(endTimeLabel);
        content.add(endTime);
        
        content.add(submit);
        submit.addActionListener(new SubmitListener());

		layout.setHgap(20);
		layout.setVgap(20);

		this.setContentPane(content);
        this.pack();
		
		this.setVisible(true);
	}
	
	public void closeWindow() {
		this.setVisible(false);
		this.dispose();
	}
	
	class SubmitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("HH:mm:ss"); 
        	
        	try {
            	minArriveData = Integer.parseInt(minArrive.getText());
            	maxArriveData = Integer.parseInt(maxArrive.getText());
            	minServiceTimeData = Integer.parseInt(minServiceTime.getText());
            	maxServiceTimeData = Integer.parseInt(maxServiceTime.getText());
            	numberOfQueuesData = Integer.parseInt(numberOfQueues.getText());
            	
        		startDate = simpleDateFormat.parse(startTime.getText());
        		endDate = simpleDateFormat.parse(endTime.getText()); 
            	
        		if (endDate.before(startDate)) {
        			throw new Exception("End time before start time");
        		}
        		
        		if (minArriveData >= maxArriveData) {
        			throw new Exception("Minimum>maximum interval of arriving time");
        		}
        		
        		if (minServiceTimeData >= maxServiceTimeData) {
        			throw new Exception("Minimum>maximum service time");
        		}
        		
                closeWindow();
            } catch (NumberFormatException nfex) {
            	JOptionPane.showMessageDialog(new JFrame(), "Not parseable integer values", "Error", JOptionPane.ERROR_MESSAGE);
            } catch (java.text.ParseException pex) {
            	JOptionPane.showMessageDialog(new JFrame(), "Not parseable start/end time", "Error", JOptionPane.ERROR_MESSAGE);
            } catch (Exception ex) {
            	JOptionPane.showMessageDialog(new JFrame(), ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

	public int getMinArriveData() {
		return minArriveData;
	}

	public int getMaxArriveData() {
		return maxArriveData;
	}

	public int getMinServiceTimeData() {
		return minServiceTimeData;
	}
	
	public int getMaxServiceTimeData() {
		return maxServiceTimeData;
	}

	public int getNumberOfQueuesData() {
		return numberOfQueuesData;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}
}
