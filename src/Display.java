import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Display extends JFrame implements Runnable {
	private ClientGenerator model;
	private int numberOfQueues;
	
	private List<JLabel> queueNameList = new ArrayList<JLabel>();
	private List<JPanel> panelList = new ArrayList<JPanel>();
	
	public Display(ClientGenerator model) {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Simulation");
		this.model = model;
		numberOfQueues=model.getNumberOfQueues();
		this.setSize(1000, 150*numberOfQueues);
		GridLayout layout = new GridLayout(numberOfQueues,1);
        this.setLayout(layout);
		
 		for (int i=0; i<numberOfQueues; i++) {
 			panelList.add(new JPanel());
 			FlowLayout innerLayout = new FlowLayout(FlowLayout.LEFT);
 			innerLayout.setVgap(25);
 			panelList.get(i).setLayout(innerLayout);
 			queueNameList.add(new JLabel(model.getQueueName(i)));
 			panelList.get(i).add(queueNameList.get(i));
 			this.add(panelList.get(i));
 		}
		this.setVisible(true);
	}

	@Override
	public void run() {
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		while (model.getRunningState()) {
			for (int i=0; i<numberOfQueues; i++) {
				panelList.get(i).removeAll();
				panelList.get(i).add(queueNameList.get(i));
				panelList.get(i).add(new JLabel(df.format(model.getBusyUntil(i))));
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for (Client client : model.getQueueList().get(i).getClientList()) {
					JButton button = new JButton(client.toString());
					if (model.getMaxServiceTime() / model.getMinServiceTime()<5)
						button.setPreferredSize(new Dimension(150 * client.getTService() / model.getMinServiceTime(), 25));
					panelList.get(i).add(button);
				}
	 			this.setContentPane(panelList.get(i));
	 		}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int i=0; i<numberOfQueues; i++) {
			panelList.get(i).removeAll();
			panelList.get(i).add(queueNameList.get(i));
 			panelList.get(i).add(new JLabel("Service time: "+model.getServiceTime(i)+"     Average waiting time: "+model.getAvgWaitingTime(i)+"     Free time: "+model.getFreeTime(i)));
 			this.setContentPane(panelList.get(i));
 			try {
 				Thread.sleep(100);
 			} catch (InterruptedException e) {
 				e.printStackTrace();
 			}
 		}
		JOptionPane.showMessageDialog(null, "Peak hour: "+df.format(model.getPeakHour()));
	}
}
